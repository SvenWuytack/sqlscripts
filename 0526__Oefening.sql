USE ModernWays;
insert into Boeken (
   Voornaam,
   Familienaam,
   Titel,
   Stad,
   Uitgeverij,
   Verschijningsjaar,
   Categorie)
values
  ('Céline', 'Claus', 'De verwondering', 'Antwerpen', 'Manteau', '1970','Filosofie'),
  ('Celine' ,'Raes', 'Jagen en gejaagd worden', 'Antwerpen', 'De Bezige Bij', '1954','Filosofie'),
  ('CELINE', 'Sarthe', 'Het zijn en het niets', 'Parijs', 'Gallimard', '1943','Filosofie');
select * from Boeken WHERE Voornaam COLLATE utf8mb4_0900_as_cs = 'Celine';
