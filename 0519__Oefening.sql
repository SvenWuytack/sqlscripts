USE ModernWays;

ALTER TABLE Liedjes ADD COLUMN Genre CHAR(20) CHAR SET utf8mb4;

SET SQL_SAFE_UPDATES = 0;
UPDATE Liedjes SET Genre = 'Hard Rock' WHERE Artiest IN ('Led Zeppelin','Van Halen');
SET SQL_SAFE_UPDATES = 1;